﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeSpace : MonoBehaviour {

	[Header("Data Values")]
	public float pulseTime;
	public float pulseRange;
	public Topic topic;

	public Transform tf;

	public float pulseTimerRemaining;

	public float lifeTimer;
	// Use this for initialization
	void Start () {
		tf = this.GetComponent<Transform> ();
		Destroy (this.gameObject, lifeTimer);

		// Z Fightig Hack
		// y is z
		Vector3 temp = tf.position;
		temp.z = temp.y * 0.001f;
		tf.position = temp;

	}
	
	// Update is called once per frame
	void Update () 
	{

		//if the pulse timer is done
		if (pulseTimerRemaining <= 0) 
		{
			//Pulse 
			DrawProtesters ();
			//Debug.Log ("Pulsed!");
			pulseTimerRemaining = pulseTime;
		} 
		else 
		{
			//otherwise countdown
			pulseTimerRemaining -= Time.deltaTime;
		}

	}

	public void DrawProtesters()
	{
		foreach (ProtesterController protester in GameManager._GAMEMANAGER.protesters) 
		{
			//for each protester in range
			if (Vector3.Distance (tf.position, protester.data.tf.position) <= pulseRange) 
			{ 
				//Debug.Log ("Distance Check");
				if (protester.topic.topic == this.topic.topic && protester.topic.isProTopic) 
				{
					//Set their targer to the safe space
					protester.SetTarget (tf.position, GameManager._GAMEMANAGER.endDelaySafeSpace);
					//Debug.Log ("New Target Set");
				}
			}
		}
	}
}
