﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NameGenerator : MonoBehaviour {

	public string[] firstNames;
	public string[] lastNames;
	public string[] twitterHandles;

	public string RandomFullHeader() { 
		string temp = "";

		temp += firstNames [UnityEngine.Random.Range (0, firstNames.Length)];
		temp += " ";
		temp += lastNames [UnityEngine.Random.Range (0, lastNames.Length)];
		temp += " ";
		temp += "@" + twitterHandles[UnityEngine.Random.Range (0, twitterHandles.Length)];

		return temp;
	}
}
