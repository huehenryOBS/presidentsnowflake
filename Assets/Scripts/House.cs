﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour {

	public Sprite[] Roofs;
	public Sprite[] Bases;
	public Sprite[] Doors;
	public Sprite[] Windows;

	public GameObject Roof;
	public GameObject Base;
	public GameObject Door;
	public GameObject Window;

	public GameObject President;
	// Use this for initialization
	void Awake()
	{
		Roof.GetComponent<SpriteRenderer> ().sprite = Roofs [Random.Range (0, Roofs.Length)];
		Base.GetComponent<SpriteRenderer> ().sprite = Bases [Random.Range (0, Bases.Length)];
		Door.GetComponent<SpriteRenderer> ().sprite = Doors [Random.Range (0, Doors.Length)];
		Window.GetComponent<SpriteRenderer> ().sprite = Windows [Random.Range (0, Windows.Length)];
		
	}

	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (President.GetComponent<Transform> ().position.x - this.GetComponent<Transform> ().position.x >= GameManager._GAMEMANAGER.MaxProtesterDistanceFromPresident) 
		{
			Destroy (this.gameObject);
		}
	}
}
