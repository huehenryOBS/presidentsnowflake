﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[Header("Components")]
	public PersonMover mover;
	public Transform tf;

	[Header("Data")]
	public float moveSpeed = 1.0f;

	// Parallel arrays for button and associated action
	[Header("Actions")]
	public List<Action> actions; 
	public List<KeyCode> btn_actions;

	[Header("DO NOT CHANGE")]
	public float shoutTimeRemaining;

	// Use this for initialization
	void Awake () {
		tf = GetComponent<Transform> ();
		mover = GetComponent<PersonMover> ();
	}

	void Start () {
		GameManager._GAMEMANAGER.player = this;
		shoutTimeRemaining = 0;

		// OBS: Pick some random actions to start
		// NEW: Start with 4 actions -- zap and 3 shouts
		//for (int i = 0; i < btn_actions.Count; i++) {
		//	actions.Add (GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard());
		//	GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
		//}

	}

	// Update is called once per frame
	void Update () {
		// Reduce shout time (if needed)
		if (shoutTimeRemaining > 0) {
			shoutTimeRemaining -= Time.deltaTime;
		}


		// Handle Movement
		Vector3 moveVector = Vector3.zero;
		moveVector += Vector3.right * Input.GetAxis ("Horizontal") * moveSpeed * Time.deltaTime;
		moveVector += Vector3.up * Input.GetAxis ("Vertical") * moveSpeed * Time.deltaTime;

		// Apply any buffs -- nerfs/environment are handled in Mover

		// If we are too far from President, change our moveVector
		if ((tf.position.x - GameManager._GAMEMANAGER.president.tf.position.x) > GameManager._GAMEMANAGER.MaxPlayerDistanceFromPresident) {
			moveVector.x = Mathf.Min (moveVector.x, 0);
		} else if ((GameManager._GAMEMANAGER.president.tf.position.x - tf.position.x) > GameManager._GAMEMANAGER.MaxPlayerDistanceFromPresident) {
			moveVector.x = Mathf.Max (0, moveVector.x);
		}
			
		mover.Move (moveVector);

		// Perform action for each button pressed
		for (int i = 0; i < btn_actions.Count ; i++) {
			if (Input.GetKeyDown (btn_actions[i])) {
				// Do the action
				if(!GameManager._GAMEMANAGER.IsOnGlobalCooldown())
				{
					actions [i].DoAction ();

				// Swap the action with a new one from the deck

				//Debug.Log (GameManager._GAMEMANAGER.IsOnGlobalCooldown());
				//actions[i] = GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard();
				//GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.Joystick1Button0)) {
			actions [0].DoAction ();
			//actions[0] = GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard();
			//GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
		}
		if (Input.GetKeyDown (KeyCode.Joystick1Button1)) {
			actions [1].DoAction ();
			//actions[1] = GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard();
			//GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
		}
		if (Input.GetKeyDown (KeyCode.Joystick1Button2)) {
			actions [2].DoAction ();
			//actions[2] = GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard();
			//GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
		}
		if (Input.GetKeyDown (KeyCode.Joystick1Button3)) {
			actions [3].DoAction ();
			//actions[3] = GameManager._GAMEMANAGER.actionsDeck.DrawRandomCard();
			//GameManager._GAMEMANAGER.UpdatePlayerActionIcons ();
		}

	}
}
