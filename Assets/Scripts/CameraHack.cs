﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHack : MonoBehaviour {

	private Transform tf;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp = tf.position;
		temp.z = -1000;
		tf.position = temp;
	}
}
