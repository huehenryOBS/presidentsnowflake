﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtesterController : MonoBehaviour {


	[Header("Info")]
	public PersonData data;
	public Topic topic;
	public float chasePlayerTimeRemaining;
	public Sprite twitterIcon;
	public string twitterName; 
	public float areaOfEffectRadius;
	public float rageModifierMax;

	[Header("DO NOT EDIT")]
	public float countdownToPositionChoice;
	public AudioSource audioSource;

	// Use this for initialization
	void Awake () {
		// Load Components
		data = GetComponent<PersonData>();
		audioSource = GetComponent<AudioSource> ();
	}
	void Start () {
		GameManager._GAMEMANAGER.protesters.Add (this);

		// Not moving at start
		data.targetPosition = data.tf.position;

		// Start countdown
		countdownToPositionChoice = data.minTimeBetweenWanders;
		chasePlayerTimeRemaining = 0;

		audioSource.pitch = 1 + Random.Range (-0.2f, 0.2f);
	}
	
	// Update is called once per frame
	void Update () {

		// Countdown our personal timers
		countdownToPositionChoice -= Time.deltaTime;

		if (chasePlayerTimeRemaining > 0) {
			chasePlayerTimeRemaining -= Time.deltaTime;
			SetTarget( GameManager._GAMEMANAGER.player.tf.position, GameManager._GAMEMANAGER.endDelayChasePlayer);
		} else {

			// If it is time to try to change position
			if (countdownToPositionChoice <= 0) {
				// Random chance to change position
				if (Random.value < data.wanderChance) {
					// Set new position
					// Choose a new lane position based on his current lane
					int maxLaneShift = Random.Range (-data.wanderDistance, data.wanderDistance); 

					while (data.targetLane + maxLaneShift < 0 || data.targetLane + maxLaneShift > GameManager._GAMEMANAGER.laneMarkerObjects.Length - 1) {
						maxLaneShift = Random.Range (-data.wanderDistance, data.wanderDistance);
					}

					data.targetLane += maxLaneShift;
					data.targetXDelta = Random.Range (-data.wanderDistanceX, data.wanderDistanceX);

					data.targetPosition = new Vector3 (data.tf.position.x + data.targetXDelta, 
						GameManager._GAMEMANAGER.laneMarkerObjects [data.targetLane].transform.position.y, 
						0);

					// Set timer to wait for minTimeBetweenWanders
					countdownToPositionChoice = data.minTimeBetweenWanders;
				} else { 
					// Set timer to wait for next decision
					countdownToPositionChoice = data.wanderDecisionTime;
				}
			}
		}

		// Set move vector as towards target position
		Vector3 moveVector = Vector3.zero;
		Vector3 vectorToTarget = (data.targetPosition - data.tf.position);
		if (vectorToTarget.magnitude > 1) {
			moveVector = (vectorToTarget.normalized) * data.wanderSpeed * Time.deltaTime;
		} 
		data.mover.Move (moveVector);

		// Update president's rage
		UpdatePresidentRage();


	}

	public void MakeAngryPresident(float rageRatio) {
		GameManager._GAMEMANAGER.president.rage += rageRatio * rageModifierMax * Time.deltaTime;
		audioSource.clip = AudioManager._AUDIOMANAGER.returnSound ("IncreaseRage");
		if (!audioSource.isPlaying) {
			audioSource.Play ();
		}

		if (rageRatio > 0.5) {
			GameManager._GAMEMANAGER.president.face.sprite = GameManager._GAMEMANAGER.president.hurtFace;
		} else if (rageRatio > 0.1) {
			GameManager._GAMEMANAGER.president.UpdateFace ();
		}
	}

	public void MakeHappyPresident(float rageRatio) {
		GameManager._GAMEMANAGER.president.rage -= rageRatio * rageModifierMax * Time.deltaTime;
		audioSource.clip = AudioManager._AUDIOMANAGER.returnSound ("DecreaseRage");
		if (!audioSource.isPlaying) {
			audioSource.Play ();
		}

		if (rageRatio > 0.1) {
			GameManager._GAMEMANAGER.president.UpdateFace ();
		}	
	}


	public void UpdatePresidentRage () {

		PresidentController president = GameManager._GAMEMANAGER.president;
		float distanceToPresident = Vector3.Distance (data.tf.position, president.tf.position);
		float rageRatio = 1 - Mathf.Clamp01(distanceToPresident / areaOfEffectRadius);

		// Set volume to rage ratio * sound volume
		audioSource.volume = rageRatio * AudioManager._AUDIOMANAGER.soundFXVolume;

		// Based on my rage
		switch (topic.topic) {
		case Topic.Topics.Aliens:
			if (topic.isProTopic) {
				// If I am pro aliens, and the president is too.
				if (president.aliens.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					// Otherwise, I am pro aliens, and the president hates them
					MakeAngryPresident(rageRatio);
				}
			} else {
				// If I am anti aliens, and the president is too.
				if (!president.aliens.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					MakeAngryPresident(rageRatio);
				}
			}
			break;
		case Topic.Topics.Rayguns:
			if (topic.isProTopic) {
				// If I am pro rayguns, and the president is too.
				if (president.rayguns.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					// Otherwise, I am pro aliens, and the president hates them
					MakeAngryPresident(rageRatio);
				}
			} else {
				// If I am anti rayguns, and the president is too.
				if (!president.rayguns.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					// I am anti rayguns and the president loves them
					MakeAngryPresident(rageRatio);
				}
			}
			break;
		case Topic.Topics.Robots:
			if (topic.isProTopic) {
				// If I am pro robots, and the president is too.
				if (president.robots.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					// Otherwise, I am pro aliens, and the president hates them
					MakeAngryPresident(rageRatio);
				}
			} else {
				// If I am anti robots, and the president is too.
				if (!president.robots.isProTopic) {
					MakeHappyPresident (rageRatio * GameManager._GAMEMANAGER.antiRageReductionRatio);
				} else {
					// I am anti aliens and the president loves them
					MakeAngryPresident(rageRatio);
				}
			}
			break;
		case Topic.Topics.President: 
				// If I am pro president
				if (topic.isProTopic) {
					MakeHappyPresident (rageRatio);
				} else {
					// else I am antiPresident
					MakeAngryPresident(rageRatio);
				}
			break;
		}

		// Make sure I didn't go out of bounds for president rage
		president.rage = Mathf.Clamp (president.rage, 0, president.maxRage);
	}




	public void SetTarget ( Vector3 targetPosition, float modTime = 0 ) { 

		// Set the target position
		data.targetPosition = targetPosition;

		// When an external source sets your target, reset timer to minTime
		if (modTime == 0) { 
			countdownToPositionChoice = data.minTimeBetweenWanders;
		} else { 
			countdownToPositionChoice = modTime;
		}
	}

	public void OnDestroy() {
		GameManager._GAMEMANAGER.protesters.Remove (this);
	}

}
