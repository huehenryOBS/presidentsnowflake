﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PresidentController : MonoBehaviour 
{
	private int chooseTopic;
	[Header("Info")]
	public PersonData data;


	[Header("Data")]
	public Topic aliens;
	public Topic robots;
	public Topic rayguns;
	public enum WalkingEnum {Walking, PrepareToMove, Moving};
	public enum PoliticalEnum {Normal, PrepareToChange, Swap};
	public Transform tf;
	public WalkingEnum walkingEnum;
	public PoliticalEnum politicalEnum;
	public PersonMover mover;
	public float rage;
	public float maxRage;
	public float minRandomTweetTime = 1.0f;
	public float maxRandomTweetTime = 3.0f;
	public float nextRandomTweetTime;


	[Header("Faces")]
	public SpriteRenderer face;
	public Sprite happyFace;
	public float happyFaceCutoff;
	public Sprite angryFace;
	public float angryFaceCutoff;
	public Sprite normalFace;
	public Sprite hurtFace;

	[Header("Movement Arrows")]
	public GameObject upArrow;
	public GameObject downArrow;
	public int currentLane;
	public bool isDoneMoving;
	public Vector3 moveTarget;
	public Vector3 moveVector;
	public float verticalMoveSpeed;
	public bool moveUp;
	public Vector3 forwardMovementVector;
	public float forwardMovementSpeed;
	[Header("Images and Objects")]
	public Sprite twitterImage;

	[Header("Moving Stage 1 to 2")]
	public float walkingTimerRemaining;
	[Header("Stances Stage 1 to 2")]
	public float stanceTimerRemaining;

	[Header("Moving Stage 2 to 3")]
	public float walkWarningTimerRemaining;
	[Header("Stances Stage 2 to 3")]
	public float stanceWarningTimerRemaining;
	// Use this for initialization
	void Start () 
	{
		GameManager._GAMEMANAGER.president = this;

		//Set all the topics to their appropriate values.
		aliens.topic = Topic.Topics.Aliens;
		if (UnityEngine.Random.value < 0.5) aliens.isProTopic = true;

		robots.topic = Topic.Topics.Robots;
		if (UnityEngine.Random.value < 0.5) robots.isProTopic = true;

		rayguns.topic = Topic.Topics.Rayguns;
		if (UnityEngine.Random.value < 0.5) rayguns.isProTopic = true;


		walkingEnum = WalkingEnum.Walking;
		politicalEnum = PoliticalEnum.Normal;
		walkingTimerRemaining = UnityEngine.Random.Range (GameManager._GAMEMANAGER.MinMoveCycleTime, GameManager._GAMEMANAGER.MaxMovingCycleTime);
		stanceTimerRemaining = UnityEngine.Random.Range (GameManager._GAMEMANAGER.MinStanceCycleTime, GameManager._GAMEMANAGER.MaxStanceCycleTime);
		walkWarningTimerRemaining = GameManager._GAMEMANAGER.MoveWarningTime;
		stanceWarningTimerRemaining = GameManager._GAMEMANAGER.CycleWarningTime;
		mover = GetComponent<PersonMover> ();
		tf = GetComponent<Transform> ();

		// Update the visuals for his opinions
		GameManager._GAMEMANAGER.UpdatePresidentOpinionIcons ();

		// Arrows off by default
		upArrow.SetActive(false);
		downArrow.SetActive (false);

		// Move him to line up with a lane
		Vector3 startPosition = tf.position;
		startPosition.y = GameManager._GAMEMANAGER.laneMarkerObjects[currentLane].position.y;
		tf.position = startPosition;

		nextRandomTweetTime = Time.time + UnityEngine.Random.Range (minRandomTweetTime, maxRandomTweetTime);
	}

	public void UpdateFace() {
		if (face != null) {
			if (rage / maxRage < happyFaceCutoff) {
				face.sprite = happyFace;
			} else if (rage / maxRage > angryFaceCutoff) {
				face.sprite = angryFace;
			} else {
				face.sprite = normalFace;
			}
		}
	}


	void RandomTweeting( ) {
		if (Time.time > nextRandomTweetTime) {
			string tweetString;

			if (rage < happyFaceCutoff) {
				tweetString = GameManager._GAMEMANAGER.proPresident[UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.proPresident.Length)];
			} else if (rage > angryFaceCutoff) {
				tweetString = GameManager._GAMEMANAGER.antiPresident[UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.antiPresident.Length)];
			} else {
				tweetString = GameManager._GAMEMANAGER.presidentRandomTweets[UnityEngine.Random.Range(0, GameManager._GAMEMANAGER.presidentRandomTweets.Length)];
			}
			Tweet (tweetString);
			nextRandomTweetTime = Time.time + UnityEngine.Random.Range (minRandomTweetTime, maxRandomTweetTime);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		// Random Tweet stuff
		RandomTweeting();

		//Stage 1 of walking
		if (walkingEnum == WalkingEnum.Walking) 
		{
			//If the timer is out
			if (walkingTimerRemaining <= 0) 
			{
				//Reset the time and move the enum to the next stage
				isDoneMoving = false;
				walkingEnum = WalkingEnum.PrepareToMove;
				walkingTimerRemaining = UnityEngine.Random.Range(GameManager._GAMEMANAGER.MinMoveCycleTime, GameManager._GAMEMANAGER.MaxMovingCycleTime);
				chooseUpOrDown ();
			} 
			else 
			{
				//Countdown the timer
				walkingTimerRemaining -= Time.deltaTime;
				Vector3 forwardMovementVector  = Vector3.right * Time.deltaTime * forwardMovementSpeed;
				mover.Move (forwardMovementVector);
			}
		}
		//Stage 1 of Stances
		if (politicalEnum == PoliticalEnum.Normal) 
		{
			//If the timer is out
			if (stanceTimerRemaining <= 0) 
			{
				//Reset the time and move the enum to the next stage

				politicalEnum = PoliticalEnum.PrepareToChange;
				stanceTimerRemaining = UnityEngine.Random.Range (GameManager._GAMEMANAGER.MinStanceCycleTime, GameManager._GAMEMANAGER.MaxStanceCycleTime);
				ChangeFeelings ();
				Debug.Log ("Change Feelings");
				//TODO: set the icons to questoin marks

			} 
			else 
			{
				//Countdown the timer
				stanceTimerRemaining -= Time.deltaTime;
			}
		}

		//Stage 2 of walking
		if (walkingEnum == WalkingEnum.PrepareToMove) 
		{
			//If the timer is out
			if (walkWarningTimerRemaining <= 0) 
			{
				//Reset the time and move the enum to the next stage

				walkingEnum = WalkingEnum.Moving;
				walkWarningTimerRemaining = GameManager._GAMEMANAGER.MoveWarningTime;
				if(moveUp == true)
				{
					MoveUp();
				}
				if(moveUp == false)
				{
					MoveDown();
				}
			} 
			else 
			{
				//Countdown the timer
				walkWarningTimerRemaining -= Time.deltaTime;
				Vector3 forwardMovementVector = Vector3.right * Time.deltaTime * forwardMovementSpeed;
				mover.Move (forwardMovementVector);

			}
		}
		//Stage 2 of Stances
		if (politicalEnum == PoliticalEnum.PrepareToChange) 
		{
			//If the timer is out
			if (stanceWarningTimerRemaining <= 0) 
			{
				//Reset the time and move the enum to the next stage

				politicalEnum = PoliticalEnum.Swap;
				stanceWarningTimerRemaining = GameManager._GAMEMANAGER.CycleWarningTime;;

				//TODO: update the icons
			} 
			else 
			{
				//Countdown the timer
				stanceWarningTimerRemaining -= Time.deltaTime;
			}
		}

		//Stage 3 of walking
		if (walkingEnum == WalkingEnum.Moving) 
		{
			if (isDoneMoving == true) 
			{
				//Move the enum to the first stage
				walkingEnum = WalkingEnum.Walking;
			} 
			else 
			{
				mover.Move (moveVector * verticalMoveSpeed * Time.deltaTime);

				if (Mathf.Abs(Vector3.Distance(tf.position , moveTarget)) <= .1f) 
				{
					isDoneMoving = true;
				}
			}
		}
		//Stage 3 of Stances

		if (politicalEnum == PoliticalEnum.Swap) 
		{
				//Move the enum to the first stage
				politicalEnum = PoliticalEnum.Normal;
			UpdateFeelings ();
			Debug.Log ("Update Feelings");
		}
	}


	public void Tweet (string textContent) {
		TweetData newTweet = new TweetData ();
		newTweet.icon = twitterImage;
		newTweet.handle = "President Snowflake @RealSnowflake - "+DateTime.Now.Hour+":"+DateTime.Now.Minute+":"+DateTime.Now.Second;
		newTweet.textContent = textContent;

		GameManager._GAMEMANAGER.tweetStream.Add (newTweet);
		GameManager._GAMEMANAGER.UpdateTwitterStream ();
	}

	public void MoveUp()
	{
		if (currentLane >= 1) {
			moveTarget = new Vector3 (tf.position.x, GameManager._GAMEMANAGER.laneMarkerObjects [currentLane - 1].position.y, 0);
			moveVector = Vector3.Normalize (moveTarget - tf.position) * verticalMoveSpeed;
			currentLane -= 1;
			upArrow.SetActive (false);
		}

	}

	public void MoveDown()
	{
		if (currentLane < GameManager._GAMEMANAGER.laneMarkerObjects.Length - 1) {
			moveTarget = new Vector3 (tf.position.x, GameManager._GAMEMANAGER.laneMarkerObjects [currentLane + 1].position.y, 0);
			moveVector = Vector3.Normalize (moveTarget - tf.position) * verticalMoveSpeed;
			currentLane += 1;
			downArrow.SetActive (false);
		}
	}
	public void chooseUpOrDown()
	{
		if (currentLane == 0) 
		{
			moveUp = false;
			downArrow.SetActive (true);
			//Debug.Log ("Already at the top, forcing down");
		}
		else if (currentLane == GameManager._GAMEMANAGER.laneMarkerObjects.Length - 1) 
		{
			moveUp = true;
			upArrow.SetActive (true);
			//Debug.Log ("Already at the bottom, forcing up");
		} 
		else 
		{
			int choose = UnityEngine.Random.Range (0, 2);
			if (choose == 0) 
			{
				moveUp = false;
				downArrow.SetActive (true);
				//Debug.Log ("Randomly Going down");
			}
			if (choose == 1) 
			{
				moveUp = true;
				upArrow.SetActive (true);
				//Debug.Log ("Randomly Going up");
			}
		}

	}
	public void ChangeFeelings()
	{
		chooseTopic = UnityEngine.Random.Range (0, 3);

		//Robots
		if (chooseTopic == 0) 
		{
			if (robots.isProTopic == true) 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.antiRobot.Length);
				Tweet(GameManager._GAMEMANAGER.antiRobot[tweetIndex] + " #NObots");
			} 
			else 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.proRobot.Length);
				Tweet(GameManager._GAMEMANAGER.proRobot[tweetIndex] + " #PRObots");
			}

			GameManager._GAMEMANAGER.HidePresidentOpinion (Topic.Topics.Robots);
		}

		//Aliens
		if (chooseTopic == 1) 
		{
			if (aliens.isProTopic == true) 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.antiAlien.Length);
				Tweet(GameManager._GAMEMANAGER.antiAlien[tweetIndex] + " #ETGoHome");
			} 
			else 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.proAlien.Length);
				Tweet(GameManager._GAMEMANAGER.proAlien[tweetIndex] + " #WelcomeToEarth");
			}

			GameManager._GAMEMANAGER.HidePresidentOpinion (Topic.Topics.Aliens);
		}

		//Rayguns
		if (chooseTopic == 2) 
		{
			if (rayguns.isProTopic == true) 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.antiRaygun.Length);
				Tweet(GameManager._GAMEMANAGER.antiRaygun[tweetIndex] + " #RaygunsRaygone ");
			} 
			else 
			{
				int tweetIndex = UnityEngine.Random.Range(0,GameManager._GAMEMANAGER.proRaygun.Length);
				Tweet(GameManager._GAMEMANAGER.proRaygun[tweetIndex] + " #PewPewPew ");
			}

			GameManager._GAMEMANAGER.HidePresidentOpinion (Topic.Topics.Rayguns);
		}


		//TODO: set the icons to the appropriate values for these variables

	}

	public void UpdateFeelings()
	{
		

		//Robots
		if (chooseTopic == 0) 
		{
			if (robots.isProTopic == true) 
			{
				robots.isProTopic = false;
			} 
			else 
			{
				robots.isProTopic = true;
			}
		}

		//Aliens
		if (chooseTopic == 1) 
		{
			if (aliens.isProTopic == true) 
			{
				aliens.isProTopic = false;

			} 
			else 
			{
				aliens.isProTopic = true;

			}
		}

		//Rayguns
		if (chooseTopic == 2) 
		{
			if (rayguns.isProTopic == true) 
			{
				rayguns.isProTopic = false;

			} 
			else 
			{
				rayguns.isProTopic = true;

			}
		}

		// Update the visuals for his opinions
		GameManager._GAMEMANAGER.UpdatePresidentOpinionIcons ();

	}
}
