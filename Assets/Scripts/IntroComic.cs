﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroComic : MonoBehaviour {

	AudioSource audioSource;

	// Use this for initialization
	void Start () 
	{
		Destroy (this.gameObject, 22);

		audioSource = gameObject.AddComponent<AudioSource> () as AudioSource;
		audioSource.clip = AudioManager._AUDIOMANAGER.returnSound ("narration");
		audioSource.volume = AudioManager._AUDIOMANAGER.soundFXVolume;
		audioSource.loop = false;
		audioSource.Play ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Escape) || Input.GetKeyDown (KeyCode.Space)) {
			Destroy (this.gameObject);
		}
	}
	void OnDestroy()
	{
		if (GameManager._GAMEMANAGER != null) {
			GameManager._GAMEMANAGER.gameObject.SetActive (true);	
			Destroy (GameManager._GAMEMANAGER.gameObject);
		}
		UnityEngine.SceneManagement.SceneManager.LoadScene (3);
	}
}
