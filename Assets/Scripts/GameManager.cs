﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager _GAMEMANAGER = null;              //Static instance of GameManager which allows it to be accessed by any other script.

	[Header("Global Game Data")]
	public float animMoveCutoff = 0.01f; // If movement is less than this speed, play idle
	public float globalCooldown = 1.5f;

	public float shoutRange;
	public float chaseTime;
	public float pushDistance;
	public float endDelayChasePlayer;
	public float endDelayPushAll;
	public float endDelaySafeSpace;
	public float DroppedSafeSpaceDuration;
	public float MaxProtesterDistanceFromPresident;
	public float MaxPlayerDistanceFromPresident;
	public float antiRageReductionRatio = 0.1f;
	public float protestersPerDifficultyLevel = 10;
	public float minSafeSpaceTime = 30.0f;
	public float safeSpaceDifficultyDelta = 5;


	[Header("President Timer Data")]
	public float MinMoveCycleTime;
	public float MaxMovingCycleTime;
	public float MoveWarningTime = 2.0f;

	public float MinStanceCycleTime;
	public float MaxStanceCycleTime;
	public float CycleWarningTime = 2.0f;
	public int difficultyLevel = 0;
	public int secondsPerDifficultyLevel = 30;

	[Header("Data")]
	public Transform[] laneMarkerObjects;
	public List<TweetData> tweetStream;
	public List<Tweet> tweetObjects;
	public ActionDeck actionsDeck;
	public List<ProtesterController> protesters;
	public PlayerController player;
	public PresidentController president;
	public GameObject[] protesterPrefabs;
	public float startTime;
	public ButtonScript bs;
	[Header("Objects")]
	public Image[] cooldownImages;
	public Image[] actionImages;
	public GameObject robotSafeSpace;
	public GameObject alienSafeSpace;
	public GameObject raygunSafeSpace;
	public NameGenerator nameGen;
	public Image presProAlien;
	public Image presAntiAlien;
	public Image presProRaygun;
	public Image presAntiRaygun;
	public Image presProRobot;
	public Image presAntiRobot;
	public Image rageBarIndicator;
	public Text timerTextbox;
	public GameObject alienBubble;
	public GameObject robotBubble;
	public GameObject raygunBubble;
	public GameObject playerBubbleSpawn;
	[Header("Tweet Arrays")]
	public string[] proRobot;
	public string[] antiRobot;
	public string[] proAlien;
	public string[] antiAlien;
	public string[] proRaygun;
	public string[] antiRaygun;

	public string[] proPresident;
	public string[] antiPresident;

	public string[] presidentRandomTweets;
	public string[] randomPeopleTweets;
	[Header("DO NOT CHANGE")]
	public float globalCooldownRemaining;
	public float safeSpaceCooldownRemining;



	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (_GAMEMANAGER == null) 
		{
			//if not, set instance to this
			_GAMEMANAGER = this;
		}
		//If instance already exists and it's not this:
		else if (_GAMEMANAGER != this) 
		{
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy (gameObject); 
		}
		DontDestroyOnLoad(gameObject);

		nameGen = FindObjectOfType (typeof(NameGenerator)) as NameGenerator;
	}
	// Use this for initialization
	void Start () {
		startTime = Time.time;
		UpdateTimerbox();
		bs = this.GetComponent<ButtonScript> ();
		safeSpaceCooldownRemining = minSafeSpaceTime;
	}

	public void UpdateTimerbox () {
		timerTextbox.text = "Time Elapsed:\n"+ ElapsedTimeToString();
	}

	public string ElapsedTimeToString() {
		string temp;
		int milisecondsRemaining = (int)(Time.time*100) - (int)(startTime*100);
		int hours = milisecondsRemaining / (360000);
		milisecondsRemaining = milisecondsRemaining % 360000;
		int minutes = milisecondsRemaining / 6000;
		milisecondsRemaining = milisecondsRemaining % 6000;
		int seconds = milisecondsRemaining / 100;
		milisecondsRemaining = milisecondsRemaining % 100;

		temp = "" + hours + ":" + minutes + ":" + seconds + ":";
		if (milisecondsRemaining < 10) {
			temp += "0";
		};
		temp += milisecondsRemaining;
		return temp;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}

		// Countdown global cooldown if needed
		UpdateGlobalCooldown();

		// Update all player ability icons
		// TODO: Only do this when we change abilities
		UpdatePlayerActionIcons();

		// Update Twitter feed
		UpdateTwitterStream();

		// Update rage
		UpdatePresidentRage ();

		// Attempt to spawnProtestors
		ManageProtesters();

		// Update timerbox
		UpdateTimerbox();

		// Level up when needed
		difficultyLevel = (int)((Time.time - startTime) / secondsPerDifficultyLevel);
	}

	public void ManageProtesters () {

		if (protesters.Count < protestersPerDifficultyLevel * (difficultyLevel+1)) {

			// Find a position
			Vector3 protesterPostion = Vector3.zero;
			int lane = Random.Range(0,GameManager._GAMEMANAGER.laneMarkerObjects.Length);
			protesterPostion.y = GameManager._GAMEMANAGER.laneMarkerObjects[lane].position.y;
			protesterPostion.x = GameManager._GAMEMANAGER.president.tf.position.x + (MaxProtesterDistanceFromPresident);
			protesterPostion.x += Random.Range (-3.0f, 3.0f);
			protesterPostion.y += Random.Range (-1.0f, 1.0f);

			GameObject temp;
			// Spawn a random protester
			temp = Instantiate (protesterPrefabs [Random.Range (0, protesterPrefabs.Length)], protesterPostion, Quaternion.identity) as GameObject;
			ProtesterController newGuy = temp.GetComponent<ProtesterController> ();
			newGuy.twitterName = temp.name = nameGen.RandomFullHeader();
			newGuy.data.targetLane = lane;

		}

		// Occasionally spawn safespaces
		if (safeSpaceCooldownRemining <= 0) {
			float safeSpacePercentChance = Mathf.Max(0, (100 - (difficultyLevel * safeSpaceDifficultyDelta)) / 100);
			Debug.Log ("!"+safeSpacePercentChance+"%");
			if (Random.value < safeSpacePercentChance) {
				Vector3 safeSpacePosition = Vector3.zero;
				int lane = Random.Range(0,GameManager._GAMEMANAGER.laneMarkerObjects.Length);
				safeSpacePosition.y = GameManager._GAMEMANAGER.laneMarkerObjects[lane].position.y;
				safeSpacePosition.x = GameManager._GAMEMANAGER.president.tf.position.x + (MaxProtesterDistanceFromPresident);

				if (Random.value < 0.33f) {
					Instantiate (robotSafeSpace, safeSpacePosition, Quaternion.identity);
				} else if (Random.value < 0.66f) {
					Instantiate (alienSafeSpace, safeSpacePosition, Quaternion.identity);
				} else {
					Instantiate (raygunSafeSpace, safeSpacePosition, Quaternion.identity);
				}
			}
			safeSpaceCooldownRemining = minSafeSpaceTime;
		}



		// If a protester is too far behind the president, destroy it
		for (int i=0; i<protesters.Count; i++) {
			float distanceBehindPres = protesters [i].data.tf.position.x - president.tf.position.x;
			//Debug.Log (distanceBehindPres);
			if ( distanceBehindPres < -MaxProtesterDistanceFromPresident) {
				//Debug.Log (protesters [i].name + " must die");
				Destroy (protesters [i]);
			}
		}

	}


	public void UpdatePresidentRage () {
		rageBarIndicator.fillAmount = president.rage / president.maxRage;
		if (president.rage >= president.maxRage) 
		{
			bs.toEndScreen();
		}
	}

	public void HidePresidentOpinion (Topic.Topics topic) {
		switch (topic) {
		case Topic.Topics.Aliens:
			presAntiAlien.gameObject.SetActive (false);
			presProAlien.gameObject.SetActive (false);
			break;
		case Topic.Topics.Rayguns:
			presAntiRaygun.gameObject.SetActive (false);
			presProRaygun.gameObject.SetActive (false);
			break;
		case Topic.Topics.Robots:
			presAntiRobot.gameObject.SetActive (false);
			presProRobot.gameObject.SetActive (false);
			break;
		}
	}

	public void UpdatePresidentOpinionIcons () {
		if (president.rayguns.isProTopic) {
			presAntiRaygun.gameObject.SetActive (false);
			presProRaygun.gameObject.SetActive (true);
		} else {
			presAntiRaygun.gameObject.SetActive (true);
			presProRaygun.gameObject.SetActive (false);
		}

		if (president.robots.isProTopic) {
			presAntiRobot.gameObject.SetActive (false);
			presProRobot.gameObject.SetActive (true);
		} else {
			presAntiRobot.gameObject.SetActive (true);
			presProRobot.gameObject.SetActive (false);
		}

		if (president.aliens.isProTopic) {
			presAntiAlien.gameObject.SetActive (false);
			presProAlien.gameObject.SetActive (true);
		} else {
			presAntiAlien.gameObject.SetActive (true);
			presProAlien.gameObject.SetActive (false);
		}


	}

	public void UpdatePlayerActionIcons () {
		for (int i = 0; i < player.actions.Count; i++) {
			if (player != null && player.actions.Count > i && actionImages.Length > i) {
				actionImages [i].sprite = player.actions [i].icon;
			}
		}

	}

	public void UpdateGlobalCooldown() {
		if (globalCooldownRemaining > 0) {
			globalCooldownRemaining -= Time.deltaTime;
		}

		foreach (Image temp in cooldownImages) {
			temp.fillAmount = Mathf.Clamp(globalCooldownRemaining/globalCooldown, 0, 1);
		}

		if (safeSpaceCooldownRemining > 0) {
			safeSpaceCooldownRemining -= Time.deltaTime;
		}

	}

	public bool IsOnGlobalCooldown() {
		return (globalCooldownRemaining > 0);
	}

	public void StartGlobalCooldown() {
		globalCooldownRemaining = globalCooldown;
	}

	public void UpdateTwitterStream() {

		// Remove from the front of the queue any tweets that go beyond our object list
		while (tweetStream.Count > tweetObjects.Count) {
			tweetStream.RemoveAt (0);
		}

		// Turn all tweet objects off
		foreach (Tweet tweetObject in tweetObjects) {
			tweetObject.gameObject.SetActive (false);
		}

		// Turn back on only the ones we set/need
		for (int i=0; i<tweetStream.Count; i++) {
			tweetObjects [i].gameObject.SetActive (true);
			tweetObjects [i].data = tweetStream [i];
			tweetObjects [i].UpdateTweet ();
		}



	}







}
